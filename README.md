若遇到紅字 cocoapods diff: /../Podfile.lock: No such file or directory
> 關閉Xcode 重新輸入 pod install

## Firebase Storage 設定

[Firebase官網]:https://console.firebase.google.com/
[Firebase官網][]

[程式碼的詳細說明]:https://www.appcoda.com.tw/firebase-storage-database/
[程式碼的詳細說明][]

* [Storage設定](#storage設定)
* [Database設定](#database設定)
* [安裝FirebaseSDK](#安裝firebase-sdk)
* [實作照片上傳](#實作照片上傳)
* [將已上傳的圖片連結寫進FirebaseDatabase](#up_photo)

    * [viewDidLoad裡新增FirebaseDatabase的實體並指定位置](#viewdidload)
    * [numberOfItemsInSection內的資料](#collectionview)
    * [UIImageView要顯示的照片](#cellForItemAt)


## Storage設定
Rules 權限區塊

    service firebase.storage {
      match /b/{bucket}/o {
        match /{allPaths=**} {
          allow read, write: if request.auth != null;
        }
      }
    }

更改為

    service firebase.storage {
      match /b/{bucket}/o {
        match /{allPaths=**} {
          allow read, write;
        }
      }
    }
## Database設定
Rules 權限區塊

      {
        "rules": {
          ".read": "auth != null",
          ".write": "auth != null"
        }
      }

更改為

    {
      "rules": {
        ".read": true,
        ".write": true
      }
    }
## 安裝Firebase SDK
1.下載下來的 GoogleService-Info.plist 拉進 Xcode 專案。

2.使用cocoapod 安裝
>cocoapod start   

    pod init
    
> 使用vim開啟Podfile 
    
    vim Podfile
    
> 輸入需要安裝的SDK，如無法輸入請按 I 鍵，輸入完畢後按esc，並輸入：wq 儲存關閉檔案
    
    Firebase/Storage
    Firebase/Database
> 最後輸入 pod install 讓CocoaPods下載並安裝相關SDK。

    pod install
3.在 Xcode 專案的 AppDelegate 中加入 Firebase 的啟動設定
    
    import UIKit
    import Firebase

    @UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate {

      var window: UIWindow?

      func application(_ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
        -> Bool {
        
        FirebaseApp.configure()
        
        return true
      }
    }

4. 確認 Info.plist 裡是否有加入兩個 Information Property List，分別是：

        Privacy – Camera Usage Description
        Privacy – Photo Library Usage Description
        
## 實作照片上傳
先在 ViewController.swift 引入 FirebaseStorage
    
    import FirebaseStorage
更改 selectedImage 的 if 判斷式

    if let selectedImage = selectedImageFromPicker {

        let storageRef = Storage.storage().reference().child("AppCodaFireUpload").child("\(uniqueString)")

        if let uploadData = UIImagePNGRepresentation(selectedImage) {
            // 這行就是 FirebaseStorage 關鍵的存取方法。
            storageRef.putData(uploadData, metadata: nil, completion: { (data, error) in

                if error != nil {

                    // 若有接收到錯誤，我們就直接印在 Console 就好，在這邊就不另外做處理。
                    print("Error: \(error!.localizedDescription)")
                    return
                }

                // 連結取得方式就是：data?.downloadURL()?.absoluteString。
                if let uploadImageUrl = data?.downloadURL()?.absoluteString {

                    // 我們可以 print 出來看看這個連結事不是我們剛剛所上傳的照片。
                    print("Photo Url: \(uploadImageUrl)")
                }
            })
        }
    }
    
<h2 id="up_photo">將已上傳的圖片連結寫進 Firebase Database</h2>
> 將上傳到 Storage 的照片網址存入 Firebase Database 方便日後管理與存取
一樣在 ViewController.swift 引入 FirebaseDatabase

    import FirebaseDatabase
修改 func imagePickerController(_:didFinishPickingMediaWithInfo:)

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        var selectedImageFromPicker: UIImage?
        // 取得從 UIImagePickerController 選擇的檔案
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImageFromPicker = pickedImage
        }
        // 可以自動產生一組獨一無二的 ID 號碼，方便等一下上傳圖片的命名
        let uniqueString = NSUUID().uuidString
        // 當判斷有 selectedImage 時，我們會在 if 判斷式裡將圖片上傳
        if let selectedImage = selectedImageFromPicker {
        
            let storageRef = Storage.storage().reference().child("AppCodaFireUpload").child("\(uniqueString)")
            if let uploadData = UIImagePNGRepresentation(selectedImage) {
                // 這行就是 FirebaseStroge 關鍵的存取方法。
                storageRef.putData(uploadData, metadata: nil, completion: { (data, error) in

                    if error != nil {
                        // 若有接收到錯誤，我們就直接印在 Console 就好，在這邊就不另外做處理。
                        print("Error: \(error!.localizedDescription)")
                        return
                    }

                    // 連結取得方式就是：data?.downloadURL()?.absoluteString。
                    if let uploadImageUrl = data?.downloadURL()?.absoluteString {

                        // 我們可以 print 出來看看這個連結事不是我們剛剛所上傳的照片。
                        print("Photo Url: \(uploadImageUrl)")

                        let databaseRef = Database.database().reference().child("AppCodaFireUpload").child(uniqueString)

                        databaseRef.setValue(uploadImageUrl, withCompletionBlock: { (error, dataRef) in
                            if error != nil {
                                print("Database Error: \(error!.localizedDescription)")
                            }
                            else {
                                print("圖片已儲存")
                            }
                        })
                    }
                })
            }
        }
        dismiss(animated: true, completion: nil)
    }
<h3 id="viewdidload">在 FireCollectionViewController 裡的 viewDidLoad 裡新增 Firebase Database 的實體並指定位置，再從 observe 的方法裡取得我們放在資料庫裡的檔案：</h3>

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let databaseRef = Database.database().reference().child("AppCodaFireUpload")
        databaseRef.observe(.value, with: { [weak self] (snapshot) in
            
            if let uploadDataDic = snapshot.value as? [String:Any] {
                self?.fireUploadDic = uploadDataDic
                self?.collectionView!.reloadData()
            }
        })
    }
<h3 id="collectionview">再來 CollectionView DataSource 的 numberOfItemsInSection 內的資料</h3>

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let dataDic = fireUploadDic {
            return dataDic.count
        }
        return 0
    }
<h3 id="cellForItemAt">最後在 CollectionView cellForItemAt 內設定 cell 上面 UIImageView 要顯示的照片：</h3>

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FireCollectionViewCell

        // Configure the cell
        if let dataDic = fireUploadDic {
            let keyArray = Array(dataDic.keys)
            if let imageUrlString = dataDic[keyArray[indexPath.row]] as? String {
                if let imageUrl = URL(string: imageUrlString) {
                    URLSession.shared.dataTask(with: imageUrl, completionHandler: { (data, response, error) in
                        if error != nil {
                            print("Download Image Task Fail: \(error!.localizedDescription)")
                        }
                        else if let imageData = data {

                            DispatchQueue.main.async {
                                cell.fireImageView.image = UIImage(data: imageData)
                            }
                        }
                    }).resume()
                }
            }
        }
        return cell
    }
